//
//  Thumbnail.swift
//  Marvel
//
//  Created by Precup Aurel Dan on 7/19/20.
//

import Foundation
struct Thumbnail: Decodable {
    let baseUrl: String
    let ext: String

    enum Size: String {
        case small = "landscape_xlarge"
        case medium = "landscape_amazing"
        case large = "landscape_incredible"
    }

    enum CodingKeys: String, CodingKey {
        case baseUrl = "path"
        case ext = "extension"
    }

    func imageUrl(forSize size: Size) -> URL? {
        return URL(string: baseUrl)?
            .appendingPathComponent(size.rawValue)
            .appendingPathExtension(ext)
    }
}
