//
//  Hero.swift
//  Marvel
//
//  Created by Precup Aurel Dan on 7/19/20.
//

import SwiftUI
struct Hero: Identifiable, Decodable {
    var id: Int = 0
    let name: String
    var description: String
    let thumbnail: Thumbnail
    let series: AvailabilityCounter
    let stories: AvailabilityCounter
    let events: AvailabilityCounter
    let comics: AvailabilityCounter
    var modified: Date? = Date()
    var heroDescription: String {
        return description.isEmpty ? "A true marvelian hero that we will never ever forget." : description
    }
}

extension Hero: Equatable {
    static func == (lhs: Hero, rhs: Hero) -> Bool {
        return lhs.id == rhs.id
    }
}
