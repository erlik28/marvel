//
//  ListPager.swift
//  Marvel
//
//  Created by Precup Aurel Dan on 7/20/20.
//

import Foundation

struct ListPager {
    private(set) var perPage = 20
    var totalRows = 0
    private(set) var offset = 0

    func hasNextPage() -> Bool {
        return offset < totalRows
    }

    mutating func increment(with rows: Int) {
        offset += rows
    }
}
