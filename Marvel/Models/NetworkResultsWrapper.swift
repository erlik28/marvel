//
//  NetworkResultsWrapper.swift
//  Marvel
//
//  Created by Precup Aurel Dan on 7/20/20.
//

import Foundation
struct NetworkResultsWrapper<T: Decodable>: Decodable {
    let code: Int
    let data: NetworkPagedData<T>
}

struct NetworkPagedData<T: Decodable>: Decodable {
    let offset: Int
    let limit: Int
    let count: Int
    let total: Int
    let results: T
}
