//
//  Date+Extension.swift
//  Marvel
//
//  Created by Precup Aurel Dan on 7/19/20.
//

import Foundation
extension Date {
    var listFormatted: String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMM"
        return dateFormatter.string(from: self)
    }

    var detailViewFormatted: String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM d, h:mm a"
        return dateFormatter.string(from: self)
    }
}

