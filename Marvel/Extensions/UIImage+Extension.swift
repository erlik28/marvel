//
//  UIImage+Extension.swift
//  Marvel
//
//  Created by Precup Aurel Dan on 7/19/20.
//

import UIKit
import SwiftUI

extension UIImage {
    var asImage: Image {
        return Image(uiImage: self)
    }
}
