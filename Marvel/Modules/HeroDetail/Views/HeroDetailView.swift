//
//  HeroDetailView.swift
//  Marvel
//
//  Created by Precup Aurel Dan on 7/19/20.
//

import SwiftUI

struct HeroDetailView: View {
    @Environment(\.imageCache) var cache: ImageCache
    var hero: Hero
    var body: some View {
        ZStackBg {
            ScrollView {
                VStack {
                    
                    URLImage(url: hero.thumbnail.imageUrl(forSize: .medium), cache: cache)
                    VStack(spacing: KSpacing.double) {

                        TitledCard(title: "About") {
                            Text(hero.heroDescription)
                                .font(.footnote)
                                .foregroundColor(KColors.secondaryText)
                        }
                        
                        NumericCardView(color: KColors.events,
                                        title: "Events",
                                        number: hero.events.available,
                                        description: "Upcoming events")

                        NumericCardView(color: KColors.stories,
                                         title: "Stories",
                                         number: hero.stories.available,
                                         description: "Stories are available")

                        NumericCardView(color: KColors.series,
                                         title: "Series",
                                         number: hero.series.available,
                                         description: "Series are available")

                        NumericCardView(color: KColors.comics,
                                         title: "Comics",
                                         number: hero.comics.available,
                                         description: "Comics are available")
                        Text("Last updated \(hero.modified?.detailViewFormatted ?? " - never")")
                            .font(.caption2)
                            .foregroundColor(KColors.tertiaryText)

                    }.padding()
                }
            }.navigationTitle(hero.name)
        }
    }
}

struct HeroDetailView_Previews: PreviewProvider {
    static var previews: some View {
        HeroDetailView(hero:  Hero(name: "Captain America", description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.", thumbnail: Thumbnail(baseUrl: "http://i.annihil.us/u/prod/marvel/i/mg/9/c0/527bb7b37ff55", ext: "jpg"), series: AvailabilityCounter(available: 124), stories: AvailabilityCounter(available: 235), events: AvailabilityCounter(available: 12), comics: AvailabilityCounter(available: 92)))
    }
}
