//
//  HeroListModel.swift
//  Marvel
//
//  Created by Precup Aurel Dan on 7/19/20.
//

import Combine
import SwiftUI

class HeroListModel: ObservableObject {

    @Published var heroes: [Hero] = []
    @Published var showEmptyView = false
    @Published var showAlert = false

    private var pager = ListPager()
    private let heroRepo: HeroRepository
    private var cancellationToken: AnyCancellable?
    init(heroRepo: HeroRepository = HeroService()) {
        self.heroRepo = heroRepo
        callingNextHeroesIfPossible()
    }

    func callingNextHeroesIfPossible() {
        guard pager.hasNextPage() || heroes.isEmpty else { return }
        cancellationToken = heroRepo.getHeroes(offset: pager.offset, perPage: pager.perPage).sink(receiveCompletion: { [weak self] (completion) in
            switch completion {
            case .failure(let error): self?.handleError(error)
            case .finished: break
            }
        }, receiveValue: { [weak self] (pagedHeroes) in
            self?.pager.totalRows = pagedHeroes.total
            self?.heroes.append(contentsOf: pagedHeroes.results)
            self?.pager.increment(with: pagedHeroes.results.count)
            self?.showEmptyView = false
        })
    }


    private func handleError(_ error: NetworkError) {
        showAlert = heroes.isEmpty
        showEmptyView = heroes.isEmpty
        print("Network error:", error)
    }
}
