//
//  HeroListView.swift
//  Marvel
//
//  Created by Precup Aurel Dan on 7/19/20.
//

import SwiftUI

struct HeroListView: View {
    @ObservedObject var viewModel: HeroListModel = HeroListModel()
    @Environment(\.imageCache) var cache: ImageCache

    var body: some View {
        NavigationView {
            ZStackBg {
                ScrollView {
                    VStack {
                        if viewModel.showEmptyView {
                            NoResultsView()
                        } else {
                            ForEach(viewModel.heroes) { hero in
                                NavigationLink(destination: HeroDetailView(hero: hero)) {
                                    HeroCard(hero: hero, cache: cache).onAppear {
                                        if hero == viewModel.heroes.last {
                                            viewModel.callingNextHeroesIfPossible()
                                        }
                                    }
                                }.buttonStyle(PlainButtonStyle())
                            }
                        }
                    }.padding()
                }
            }.navigationTitle("Heroes")
        }.alert(isPresented: $viewModel.showAlert) {
            Alert(title: Text("Ooops"), message: Text("It seems there was an error. Please try again later"), dismissButton: .default(Text("Ok")))
        }
    }
}

struct HeroListView_Previews: PreviewProvider {
    static var previews: some View {
        HeroListView()
    }
}


