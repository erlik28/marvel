//
//  HeroCard.swift
//  Marvel
//
//  Created by Precup Aurel Dan on 7/19/20.
//

import SwiftUI

struct HeroCard: View {
    let hero: Hero
    var cache: ImageCache?
    var body: some View {
        Card(padding: 0) {
            VStack {
                URLImage(url: hero.thumbnail.imageUrl(forSize: .medium), cache: cache)
                VStack(alignment: .leading) {
                    Text(hero.name)
                        .font(.headline)
                        .fontWeight(.bold)
                    Spacer().frame(height: KSpacing.double)
                    Text(hero.heroDescription)
                        .font(.subheadline)
                        .lineLimit(2)
                        .foregroundColor(KColors.secondaryText)
                    Spacer().frame(height: KSpacing.double)

                    HStack {
                        Spacer()
                        Text("Updated \(hero.modified?.listFormatted ?? " - never")")
                            .fontWeight(.semibold)
                            .foregroundColor(KColors.secondaryText)
                            .font(.footnote)
                    }

                }.padding([.leading, .trailing, .bottom], KSpacing.double)
            }
        }
    }
}

struct HeroCard_Previews: PreviewProvider {
    static var previews: some View {
        MockScreen {
            HeroCard(hero:  Hero(name: "Iron Man", description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.", thumbnail: Thumbnail(baseUrl: "http://i.annihil.us/u/prod/marvel/i/mg/9/c0/527bb7b37ff55", ext: "jpg"), series: AvailabilityCounter(available: 124), stories: AvailabilityCounter(available: 235), events: AvailabilityCounter(available: 12), comics: AvailabilityCounter(available: 92)))
        }
    }
}
