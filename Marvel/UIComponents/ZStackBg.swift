//
//  ZStackBg.swift
//  Marvel
//
//  Created by Precup Aurel Dan on 7/19/20.
//

import SwiftUI

struct ZStackBg<T: View>: View {
    var backgroundColor: Color = KColors.background
    var view: () -> T
    var body: some View {
        ZStack {
            backgroundColor.edgesIgnoringSafeArea(.all)
            view()
        }
    }
}

struct ZStackBg_Previews: PreviewProvider {
    static var previews: some View {
        ZStackBg(backgroundColor: .red) {
            Text("dsa")
        }
    }
}
