//
//  Card.swift
//  Marvel
//
//  Created by Precup Aurel Dan on 7/19/20.
//

import SwiftUI

struct Card<T: View>: View {
    @State var padding: CGFloat = KSpacing.single
    var view: () -> T
    var body: some View {
        VStack {
            HStack {
                VStack {
                    view()
                }.padding(.all, padding)
            }.background(KColors.card)
            .cornerRadius(KSpacing.radius)
        }
    }
}

struct Card_Previews: PreviewProvider {
    static var previews: some View {
        MockScreen {
            Card(padding: 0){
                VStack{
                    Image(uiImage: #imageLiteral(resourceName: "landscape_xlarge2")).resizable().scaledToFit()
                    Text("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.")
                }
            }
        }
    }
}
