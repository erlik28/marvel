//
//  CardTitle.swift
//  Marvel
//
//  Created by Precup Aurel Dan on 7/19/20.
//

import SwiftUI

struct CardTitle: View {
    let title: String
    var body: some View {
        HStack{
            Text(title)
                .font(.footnote)
                .fontWeight(.heavy)

            Spacer()
        }
    }
}
