//
//  NumericCardView.swift
//  Marvel
//
//  Created by Precup Aurel Dan on 7/19/20.
//

import SwiftUI

struct NumericCardView: View {
    let color: Color
    let title: String
    let number: Int
    let description: String
    var body: some View {
        TitledCard(title: title, contentSpacing: 0) {
            HStack(alignment: .firstTextBaseline, spacing: KSpacing.half) {
                Text("\(number)")
                    .foregroundColor(color)
                    .font(.title)
                    .fontWeight(.heavy)
                Text(description).font(.footnote)
                Spacer()
            }

        }
    }
}


struct NumericCardView_Previews: PreviewProvider {
    static var previews: some View {
        MockScreen {
            NumericCardView(color: .red, title: "Title", number: 10, description: "Some decription")
        }
    }
}
