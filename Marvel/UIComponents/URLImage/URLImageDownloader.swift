//
//  URLImageDownloader.swift
//  Marvel
//
//  Created by Precup Aurel Dan on 7/20/20.
//

import UIKit
import Combine

class URLImageDownloader: ObservableObject {
    private static let placeholderImage = #imageLiteral(resourceName: "placeholder")
    @Published var image: UIImage = URLImageDownloader.placeholderImage
    private var cache: ImageCache?
    private var loading: Bool = false
    private let url: URL?
    private var cancellable: AnyCancellable?

    init(url: URL?, cache: ImageCache?) {
        self.url = url
        self.cache = cache
        load()
    }

    func load() {
        guard !loading, let url = url else { return }

        if let img = cache?[url] {
            image = img
            return
        }

        cancellable = NetworkWorker().downloadData(fromUrl: url)
            .map { UIImage(data: $0) ?? URLImageDownloader.placeholderImage }
            .handleEvents(receiveSubscription: { [weak self] _ in self?.startLoading() },
                          receiveOutput: { [weak self] in self?.cache($0) },
                          receiveCompletion: { [weak self] _ in self?.stopLoading() })
            .replaceError(with: URLImageDownloader.placeholderImage)
            .receive(on: DispatchQueue.main)
            .assign(to: \.image, on: self)
    }


    func cache(_ image: UIImage?) {
        guard let url = url else { return }
        cache?[url] = image
    }

    func cancel() {
        cancellable?.cancel()
    }

    private func startLoading() {
        loading = true
    }

    private func stopLoading() {
        loading = false
    }
}
