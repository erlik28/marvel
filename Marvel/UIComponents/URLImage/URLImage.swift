//
//  URLImage.swift
//  Marvel
//
//  Created by Precup Aurel Dan on 7/20/20.
//

import SwiftUI

struct URLImage: View {
    @ObservedObject private var downloader: URLImageDownloader
    
    init(url: URL?, cache: ImageCache? = nil) {
        downloader = URLImageDownloader(url: url, cache: cache)
    }

    var body: some View {
        ZStack {
            Image(uiImage: downloader.image)
                .resizable()
                .scaledToFit()
        }

    }
}

struct URLImage_Previews: PreviewProvider {
    static var previews: some View {
        MockScreen {
            URLImage(url: URL(string: "https://images-na.ssl-images-amazon.com/images/I/91qvAndeVYL._RI_.jpg")!)
        }
    }
}
