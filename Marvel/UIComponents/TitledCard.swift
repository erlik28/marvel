//
//  TitledCard.swift
//  Marvel
//
//  Created by Precup Aurel Dan on 7/19/20.
//

import SwiftUI

struct TitledCard<T: View>: View {
    let title: String
    var contentSpacing = KSpacing.single
    let view: () -> T
    var body: some View {
        Card(padding: KSpacing.double) {
            VStack(spacing: contentSpacing) {
                CardTitle(title: title)
                view()
            }

        }
    }
}

struct TitledCard_Previews: PreviewProvider {
    static var previews: some View {
        MockScreen {
            TitledCard(title: "Some title", contentSpacing: 200) {
                Text("Some content")
            }
        }
    }
}
