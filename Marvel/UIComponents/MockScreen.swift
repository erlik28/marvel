//
//  MockScreen.swift
//  Marvel
//
//  Created by Precup Aurel Dan on 7/19/20.
//

import SwiftUI

struct MockScreen<T: View>: View {
    var mock: () -> T
    var body: some View {
        ZStack {
            KColors.background.edgesIgnoringSafeArea(.all)
            ScrollView {
                HStack{
                    Spacer()
                    VStack {
                        Spacer()
                        mock()
                        Spacer()
                    }
                    Spacer()
                }
            }.padding(.all, KSpacing.double)
        }
    }
}

