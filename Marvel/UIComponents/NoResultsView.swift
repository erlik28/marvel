//
//  EmptyView.swift
//  Marvel
//
//  Created by Precup Aurel Dan on 7/19/20.
//

import SwiftUI

struct NoResultsView: View {
   @State var message = "No results founds"
    var body: some View {
        VStack {
            Image.init(systemName: "questionmark.circle.fill")
                .font(.title)
            Text(message)
                .font(.largeTitle)
        }.foregroundColor(KColors.quaternaryText)
    }
}

struct EmptyView_Previews: PreviewProvider {
    static var previews: some View {
        NoResultsView()
    }
}
