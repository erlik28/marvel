//
//  Request.swift
//  Marvel
//
//  Created by Precup Aurel Dan on 7/20/20.
//

import Foundation

public protocol Request {
    var path: String { get }
    var method: NetworkMethod { get }
    var cached: Bool { get }
    var queryParameters: [String: String?]? { get }
    var payload: [String: Any]? { get }
}

public protocol DecodableRequest: Request {
    associatedtype ModelType: Decodable
}

extension Request {

    func urlRequest(with baseUrl: URL,
                    headers: [String: String] = [:]) -> URLRequest? {
        var components = URLComponents(url: baseUrl.appendingPathComponent(path), resolvingAgainstBaseURL: false)
        components?.queryItems = NetworkAuth.asQueryParams

        if let extraQueryParams = queryParameters?.map({URLQueryItem(name: $0.key, value: $0.value)}) {
            components?.queryItems?.append(contentsOf: extraQueryParams)
        }

        guard let url = components?.url else { return nil }

        var request = URLRequest(url: url)
        request.httpMethod = method.rawValue

        if let payload = payload {
            request.httpBody = try? JSONSerialization.data(withJSONObject: payload)
        }

        for (key, value) in headers {
            request.addValue(value, forHTTPHeaderField: key)
        }

        request.cachePolicy = cached ? .reloadRevalidatingCacheData : .reloadIgnoringCacheData

        return request
    }

}
