//
//  HeroRequests.swift
//  Marvel
//
//  Created by Precup Aurel Dan on 7/20/20.
//

import Foundation
struct HeroRequest: DecodableRequest {
    typealias ModelType = NetworkResultsWrapper<[Hero]>
    var path: String = "characters"
    var method: NetworkMethod = .get
    var cached: Bool = true
    var queryParameters: [String : String?]?
    var payload: [String : Any]?

}
