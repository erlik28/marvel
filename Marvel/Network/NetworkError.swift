//
//  NetworkError.swift
//  Marvel
//
//  Created by Precup Aurel Dan on 7/20/20.
//

import Foundation
public enum NetworkError: Error {
    case emptyResponse
    case decodingError(Error)
    //422
    case validationError
    //401
    case accessTokenExpired
    //400
    case badRequest
    //500
    case serverError
    //404
    case notFound
    //none of the above
    case unknownServerError(Int)
    case clientError(Error)
    case timeout
    case badServerResponse

    func toHuman() -> String {
        return "There was an error"
    }
}
