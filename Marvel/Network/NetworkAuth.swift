//
//  NetworkAuth.swift
//  Marvel
//
//  Created by Precup Aurel Dan on 7/20/20.
//

import Foundation
import CryptoKit

struct NetworkAuth {
    static let privatekey = "5988398be5b8ed9e2f377c08cb50d414dd054640"
    static let apikey = "5ba3a348b1bcf94d616279390e00c82e"
    static let ts = Date().timeIntervalSince1970.description
    static let hash = Insecure.MD5.hash(data: "\(ts)\(privatekey)\(apikey)".data(using: .utf8)!)
        .map { String(format: "%02hhx", $0) }.joined()
    static let asQueryParams: [URLQueryItem] = [URLQueryItem(name: "apikey", value: apikey),
                                                URLQueryItem(name: "ts", value: ts),
                                                URLQueryItem(name:  "hash", value: hash)]
}
