//
//  NetworkMethod.swift
//  Marvel
//
//  Created by Precup Aurel Dan on 7/20/20.
//

import Foundation

public enum NetworkMethod: String {
    case get = "GET"
    case post = "POST"
    case put = "PUT"
    case patch = "PATCH"
    case delete = "DELETE"
}
