//
//  NetworkWorker.swift
//  Marvel
//
//  Created by Precup Aurel Dan on 7/20/20.
//

import Foundation
import Combine

protocol NetworkWorkerProtocol {
    var baseUrl: URL { get }
    func request<R: DecodableRequest>(_ request: R) -> AnyPublisher<R.ModelType, NetworkError>
    func downloadData(fromUrl: URL) -> AnyPublisher<Data, NetworkError>
}

class NetworkWorker: NetworkWorkerProtocol {
    internal let baseUrl = URL(string: "https://gateway.marvel.com:443/v1/public")!
    private let networkEngine: TransportEngine

    init(networkEngine: TransportEngine = NetworkTransportEngine()) {
        self.networkEngine = networkEngine
    }

    func request<T: DecodableRequest>(_ request: T) -> AnyPublisher<T.ModelType, NetworkError> {
        guard let urlRequest = request.urlRequest(with: baseUrl, headers: headers(for: request)) else {
            return Result<T.ModelType, NetworkError>.Publisher(.badRequest).eraseToAnyPublisher()
        }

        return networkEngine.perform(urlRequest)
    }

    func downloadData(fromUrl url: URL) -> AnyPublisher<Data, NetworkError> {
        return networkEngine.downloadDataFrom(url: url)
    }

}

// MARK: - Headers
extension NetworkWorker {

    private func headers(for request: Request) -> [String: String] {
        return ["Content-Type": "application/json", "Accept": "application/json"]
    }
}
