//
//  NetworkTransportEngine.swift
//  Marvel
//
//  Created by Precup Aurel Dan on 7/20/20.
//

import Foundation
import Combine
import UIKit

protocol TransportEngine {
    func perform<T: Decodable>(_ request: URLRequest) -> AnyPublisher<T, NetworkError>
    func downloadDataFrom(url: URL) -> AnyPublisher<Data, NetworkError>
}

struct NetworkTransportEngine: TransportEngine {
    private let imageDataCache = NSCache<NSString, UIImage>()
    func perform<T: Decodable>(_ request: URLRequest) -> AnyPublisher<T, NetworkError> {
        let decoder = getJSONDecoder()
        return URLSession.shared
            .dataTaskPublisher(for: request)
            .tryMap() { element -> Data in
                guard let httpResponse = element.response as? HTTPURLResponse else {
                    throw NetworkError.badServerResponse
                }

                guard (200...299).contains(httpResponse.statusCode) else {
                    throw self.errorFromResponseCode(httpResponse.statusCode)
                }

                return element.data
            }
            .decode(type: T.self, decoder: decoder)
            .receive(on: RunLoop.main)
            .mapError { (error) -> NetworkError in
                if let networkErr = error as? NetworkError {
                    return networkErr
                } else {
                    return NetworkError.clientError(error)
                }
            }
            .eraseToAnyPublisher()
    }

    func downloadDataFrom(url: URL) -> AnyPublisher<Data, NetworkError> {
        return URLSession.shared.dataTaskPublisher(for: url)
            .tryMap() { element -> Data in
                guard let httpResponse = element.response as? HTTPURLResponse else {
                    throw NetworkError.badServerResponse
                }

                guard (200...299).contains(httpResponse.statusCode) else {
                    throw self.errorFromResponseCode(httpResponse.statusCode)
                }

                return element.data
            }
            .mapError { (error) -> NetworkError in
                if let networkErr = error as? NetworkError {
                    return networkErr
                } else {
                    return NetworkError.clientError(error)
                }
            }
            .eraseToAnyPublisher()
    }

    private func getJSONDecoder() -> JSONDecoder {
        let decoder = JSONDecoder()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        decoder.dateDecodingStrategy = .formatted(dateFormatter)
        return decoder
    }
}

// MARK: - Error Handling
extension NetworkTransportEngine {

    private func errorFromResponseCode(_ code: Int) -> NetworkError {
        switch code {
        case 422:
            return .validationError
        case 401:
            return .accessTokenExpired
        case 400:
            return .badRequest
        case 404:
            return .notFound
        case 500:
            return .serverError
        default:
            return .unknownServerError(code)
        }
    }

}

