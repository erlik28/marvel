//
//  Constants.swift
//  Marvel
//
//  Created by Precup Aurel Dan on 7/19/20.
//

import UIKit
import SwiftUI

struct KColors {
    static let background: Color = Color(UIColor.secondarySystemFill)
    static let card: Color = Color(UIColor.systemBackground)
    static let primaryText: Color = Color(UIColor.label)
    static let secondaryText: Color = Color(UIColor.secondaryLabel)
    static let tertiaryText: Color = Color(UIColor.tertiaryLabel)
    static let quaternaryText: Color = Color(UIColor.quaternaryLabel)

    static let events: Color = Color(UIColor(named: "eventsColor")!)
    static let stories: Color = Color(UIColor(named: "storiesColor")!)
    static let series: Color = Color(UIColor(named: "seriesColor")!)
    static let comics: Color = Color(UIColor(named: "comicsColor")!)
}

struct KSpacing {
    static let radius: CGFloat = 9
    static let half: CGFloat = 4
    static let single: CGFloat = 8
    static let double: CGFloat = 16
    static let triple: CGFloat = 24
}
