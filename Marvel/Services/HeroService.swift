//
//  HeroService.swift
//  Marvel
//
//  Created by Precup Aurel Dan on 7/19/20.
//

import Combine

protocol HeroRepository {
    func getHeroes(offset: Int, perPage: Int) -> AnyPublisher<NetworkPagedData<[Hero]>, NetworkError>
}
class HeroService: HeroRepository {
    private let worker: NetworkWorkerProtocol

    init(worker: NetworkWorkerProtocol = NetworkWorker()) {
        self.worker = worker
    }

    func getHeroes(offset: Int, perPage: Int) -> AnyPublisher<NetworkPagedData<[Hero]>, NetworkError> {
        let request = HeroRequest(queryParameters: ["offset": "\(offset)", "limit": "\(perPage)" ])
        return worker.request(request)
            .map({$0.data})
            .eraseToAnyPublisher()
    }

}
