//
//  MarvelApp.swift
//  Marvel
//
//  Created by Precup Aurel Dan on 7/19/20.
//

import SwiftUI

@main
struct MarvelApp: App {
    var body: some Scene {
        WindowGroup {
            HeroListView()
        }
    }
}
