//
//  MockHeroService.swift
//  MarvelTests
//
//  Created by Precup Aurel Dan on 7/20/20.
//

import Foundation
@testable import Marvel
import Combine

final class MockHeroService: HeroRepository {
    var error: NetworkError?
    var data = NetworkPagedData(offset: 0, limit: 20, count: 20, total: 2000, results: [
        Hero(name: "Iron Man", description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.", thumbnail: Thumbnail(baseUrl: "http://i.annihil.us/u/prod/marvel/i/mg/9/c0/527bb7b37ff55", ext: "jpg"), series: AvailabilityCounter(available: 124), stories: AvailabilityCounter(available: 235), events: AvailabilityCounter(available: 12), comics: AvailabilityCounter(available: 92)),
        Hero(name: "Thor", description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.", thumbnail: Thumbnail(baseUrl: "http://i.annihil.us/u/prod/marvel/i/mg/9/c0/527bb7b37ff55", ext: "jpg"), series: AvailabilityCounter(available: 124), stories: AvailabilityCounter(available: 235), events: AvailabilityCounter(available: 12), comics: AvailabilityCounter(available: 92)),
        Hero(name: "Captain America", description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.", thumbnail: Thumbnail(baseUrl: "http://i.annihil.us/u/prod/marvel/i/mg/9/c0/527bb7b37ff55", ext: "jpg"), series: AvailabilityCounter(available: 124), stories: AvailabilityCounter(available: 235), events: AvailabilityCounter(available: 12), comics: AvailabilityCounter(available: 92))
    ])

    func getHeroes(offset: Int, perPage: Int) -> AnyPublisher<NetworkPagedData<[Hero]>, NetworkError> {
        if let error = error {
            return Fail<NetworkPagedData<[Hero]>, NetworkError>(error: error).eraseToAnyPublisher()
        }
        return Just(data)
            .mapError { (error) -> NetworkError in return .badRequest }
            .eraseToAnyPublisher()
    }
}
