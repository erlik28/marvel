//
//  HeroesListModelTests.swift
//  MarvelTests
//
//  Created by Precup Aurel Dan on 7/20/20.
//

import XCTest
@testable import Marvel

final class HeroesListModelTests: XCTestCase {
    var sut: HeroListModel?
    var repo: MockHeroService?

    override func setUpWithError() throws {
        repo = MockHeroService()
        sut = HeroListModel(heroRepo: repo!)
    }

    func testModelRetrievesAListOfModels() {
        XCTAssertEqual(repo?.data.results, sut?.heroes)
    }

    func testCallingNextPageIncreasesHerosCount() {
        XCTAssertEqual(repo?.data.results, sut?.heroes)
        sut?.callingNextHeroesIfPossible()
        XCTAssertEqual((repo?.data.results.count)! * 2, sut?.heroes.count)
    }

    func testEmptyViewOnlyShowsWhenHerosArrayEmpty() {
        repo?.error = .badRequest
        sut = HeroListModel(heroRepo: repo!)
        XCTAssertTrue(sut!.showEmptyView)
    }

    func testEmptyViewNotShownIfArrayNotEmpty() {
        sut = HeroListModel(heroRepo: repo!)
        repo?.error = .badRequest
        XCTAssertFalse(sut!.showEmptyView)
    }

    func testModelShowsAlertOnRequestError() {
        repo?.error = .badRequest
        sut = HeroListModel(heroRepo: repo!)
        XCTAssertTrue(sut!.showAlert)
    }

}
