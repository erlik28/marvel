//
//  HeroTests.swift
//  MarvelTests
//
//  Created by Precup Aurel Dan on 7/20/20.
//

import XCTest
@testable import Marvel

final class HeroTests: XCTestCase {
    var sut: Hero?
    override func setUpWithError() throws {
        sut = Hero(name: "Iron Man", description: "Lorem ipsum dolor sit amet.",
                   thumbnail: Thumbnail(baseUrl: "http://i.annihil.us/u/prod/marvel/i/mg/9/c0/527bb7b37ff55",
                                        ext: "jpg"),
                   series: AvailabilityCounter(available: 124),
                   stories: AvailabilityCounter(available: 235),
                   events: AvailabilityCounter(available: 12),
                   comics: AvailabilityCounter(available: 92))
    }

    func testEmptyDescriptionReturnsString() {
        sut?.description = ""
        XCTAssertEqual(sut!.heroDescription, "A true marvelian hero that we will never ever forget.")
    }

    func testNilDescriptionReturnsString() {
        let testDescription = "some decription"
        sut?.description = testDescription
        XCTAssertEqual(sut!.heroDescription, testDescription)
    }
}
