//
//  PagerTests.swift
//  MarvelTests
//
//  Created by Precup Aurel Dan on 7/20/20.
//

import XCTest
@testable import Marvel

final class PagerTests: XCTestCase {
    var sut: ListPager?

    override func setUpWithError() throws {
        sut = ListPager()
    }

    func testAllowNextPageIfThereAreStillRowsToDownload() {
        sut?.totalRows = 100
        sut?.increment(with: 10)
        XCTAssertTrue(sut!.hasNextPage())
    }

    func testDoesnAllowNextPageIfThereOnLastRow() {
        sut?.totalRows = 100
        sut?.increment(with: 100)
        XCTAssertFalse(sut!.hasNextPage())
    }

    func testIncrementIncreasesTheOffset() {
        XCTAssertEqual(sut!.offset, 0)
        sut?.increment(with: 10)
        XCTAssertEqual(sut!.offset, 10)
    }
}
