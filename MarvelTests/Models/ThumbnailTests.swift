//
//  ThumbnailTests.swift
//  MarvelTests
//
//  Created by Precup Aurel Dan on 7/20/20.
//

@testable import Marvel
import XCTest

final class ThumbnailTests: XCTestCase {
    var sut: Thumbnail?
    private let testPath = "http://some-path.com/image-id-here"
    private let fileExtension = "jpg"
    override func setUpWithError() throws {
        sut = Thumbnail(baseUrl: testPath, ext: "jpg")
    }

    func testUrlForSmallSize() {
        XCTAssertEqual(sut?.imageUrl(forSize: .small)?.absoluteString, "http://some-path.com/image-id-here/\(Thumbnail.Size.small.rawValue).\(fileExtension)")
    }

    func testUrlForMediumSize() {
        XCTAssertEqual(sut?.imageUrl(forSize: .medium)?.absoluteString, "http://some-path.com/image-id-here/\(Thumbnail.Size.medium.rawValue).\(fileExtension)")
    }

    func testUrlForLargeSize() {
        XCTAssertEqual(sut?.imageUrl(forSize: .large)?.absoluteString, "http://some-path.com/image-id-here/\(Thumbnail.Size.large.rawValue).\(fileExtension)")
    }
}
